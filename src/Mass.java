import java.awt.*;

class Mass {
    private double mass;
    private double dropHeight;
    private int diameter;
    private final Vector initialPosition;
    private Vector acceleration, velocity;
    Vector position = new Vector(0, 0);
    double record = 0;
    int intRadius, recordInt, lastDistance = 0;
    boolean airBorne;
    private Vector airResistance;

    Mass(double mass, double diameter, Vector initial){
        this.mass = mass;
        this.initialPosition = initial;
        setDiameter(diameter);

        initializeVectors();
    }

    void initializeVectors(){
        airBorne = true;
        acceleration = new Vector(0, 0);
        velocity = new Vector(0, 0);
        airResistance = new Vector(0, 0);
        position.setX(initialPosition.getX());
        position.setY(initialPosition.getY());
    }

    private double integrate(double output, double integrand){
        return output + integrand * Simulation.samplePeriod;
    }

    void calculateMovement(Vector force){
        if (position.getY() + velocity.getY() * Simulation.samplePeriod <= dropHeight) {
            position.setY(dropHeight);

            if (velocity.getY() < -2) {
                velocity.setY(Math.sqrt(velocity.getY() * velocity.getY() - 4)); // ground absorbs some kinetic energy when bouncing
            } else {
                velocity.setY(0);
                velocity.setX(velocity.getX() * 0.99);
            }

            if (airBorne) {
                double distance = position.getX();
                if (distance > record){
                    System.out.print("new record, ");
                    Simulation.recordField.setText(String.format("%.2f", distance));
                    record = distance;
                    recordInt = (int) Math.round(Simulation.DISTANCE_SCALE * record);
                }

                lastDistance = (int) Math.round(Simulation.DISTANCE_SCALE * distance);
                System.out.printf("distance = %.2f m\n", distance);
                Simulation.scoreField.setText(String.format("%.2f", distance));
                Simulation.northPanel.setVisible(true);
                airBorne = false;
            }

        } else {
            acceleration.setX((force.getX() + airResistance.getX()) / mass);
            acceleration.setY((force.getY() + airResistance.getY()) / mass - 9.81);

            // Get the velocity
            velocity.setX(integrate(velocity.getX(), acceleration.getX()));
            velocity.setY(integrate(velocity.getY(), acceleration.getY()));
            velocity.setDirection();
            velocity.setAbs();

//            double C_d = 0.3;
            airResistance.setAbs(0.25 * velocity.getAbs() * velocity.getAbs());
            airResistance.setDirection(velocity.getDirection() - Math.PI);
            airResistance.setXY();

            // Get the y position
            position.setY(integrate(position.getY(), velocity.getY()));
        }

        position.setX(integrate(position.getX(), velocity.getX()));
    }

    public void setMass(double mass){
        this.mass = mass;
    }

    public void setDiameter(double diameter) {
        this.diameter = (int) Math.round(Simulation.DISTANCE_SCALE * diameter);
        dropHeight = Simulation.groundHeight + 0.5 * this.diameter / Simulation.DISTANCE_SCALE;
        intRadius = (int) Math.round(0.5 * this.diameter);
    }

    void draw(Graphics2D g2d) {
        int[] coordinates = position.toPixelIndices(Simulation.yBound);
        g2d.fillOval(coordinates[0] - intRadius, coordinates[1] - intRadius, diameter, diameter);
    }

    public void changeMass(int value) {
        if (!Simulation.timer.isRunning()) {
            double mass = Double.parseDouble(Simulation.sizeField.getText());
            if ((mass > 1 && value < 0) || (mass < 20 && value > 0)) {
                double newMass = mass + value;
                Simulation.sizeField.setText("" + newMass);

                setMass(newMass);
                setDiameter(Math.sqrt(0.05 * newMass));
                Simulation.timer.start();
                RetryButton.restarted = true;

            }
        }
    }

    public Vector getVelocity() {
        return velocity;
    }
}
