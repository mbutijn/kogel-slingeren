import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.GradientPaint;

public class Simulation {
    static Timer timer;
    static final int groundHeight = 2, throwZoneWidth = 4, throwZoneHeight = 10, space_left = 2; // in game distances, m
    private final static int xBound = 1600; // resolution
    static int yBound = 900;
    static final int DISTANCE_SCALE = (int) Math.round(xBound / 32.0); // field is 32 meter wide, and 24 meter high
    static final int GROUND_HEIGHT_DRAW = yBound - groundHeight * DISTANCE_SCALE;
    static final int THROW_ZONE_HEIGHT = yBound - throwZoneHeight * DISTANCE_SCALE;
    static final int SPACING_LEFT = space_left * DISTANCE_SCALE;
    static final private Vector mouseOffset = new Vector(3, 28);
    static double samplePeriod = 0.02; // s -> f_s = 50 Hz
    static Mass mass = new Mass(5.0, 0.5, new Vector(space_left + 2, groundHeight + 3));
    static MouseControl mouseControl;
    static AI ai = new AI(false, mass);
    private final ThrowField throwField = new ThrowField();
    public static JButton up, down, retryButton, restartButton;
    SpringDamper springDamper = new SpringDamper(1.0, -60, -5);
    static TextField scoreField = new TextField("0", 3);
    static TextField recordField = new TextField("0", 3);
    static TextField sizeField = new TextField("5.0", 3);
    static JPanel northPanel;
    static Font font = new Font("SansSerif", Font.BOLD, 20);

    public static void main(String args[]) {
        new Simulation().makeUI();
    }

    private void makeUI() {
        timer = new Timer((int) (1000 * samplePeriod), update);

        JFrame frame = new JFrame("Kogel slingeren");
        frame.setVisible(true);
        frame.setSize(xBound, yBound);
        frame.setContentPane(throwField);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mouseControl = new MouseControl(frame, mouseOffset);

        // The score text field
        northPanel = new JPanel();
        northPanel.add(setLabel("Previous score: "));
        scoreField.setFont(font);
        scoreField.setEditable(false);
        northPanel.add(scoreField);
        northPanel.add(setLabel("m"));

        northPanel.add(setLabel("Record: "));
        recordField.setEditable(false);
        recordField.setFont(font);
        northPanel.add(recordField);
        northPanel.add(setLabel("m"));

        northPanel.add(setLabel("mass: "));
        sizeField.setEditable(true);
        sizeField.setFont(font);
        northPanel.add(sizeField);
        northPanel.add(setLabel("kg"));

        JPanel modifyPanel = new JPanel(new GridLayout(2, 1));
        up = new ModifyButton("+", 1).makeButton();
        down = new ModifyButton("-", -1).makeButton();
        modifyPanel.add(up);
        modifyPanel.add(down);
        northPanel.add(modifyPanel);

        // The restart button
        retryButton = new RetryButton(this).makeButton();
        northPanel.add(retryButton);

        restartButton = new RestartButton(this).makeButton();
        northPanel.add(restartButton);

        frame.getContentPane().add(northPanel, BorderLayout.NORTH);
        northPanel.setVisible(true);

        timer.setRepeats(true);
    }

    private Label setLabel(String name){
        Label label = new Label(name);
        label.setFont(font);
        return label;
    }

    private final ActionListener update = new ActionListener() {
        Vector force = new Vector(0,0);
        public void actionPerformed(ActionEvent evt) { // runs every 10 milliseconds
            // Run one iteration after pressing restart button
            if (RetryButton.restarted){
                Simulation.timer.stop();
                mass.initializeVectors();
                RetryButton.restarted = false;
            }

            // Update the position of the mass
            mass.calculateMovement(force);

            // Update the vectors of the spring points
            if (ai.playing) {
                ai.updateAction();
                force = springDamper.updateForceAI(mass.position, ai.getPressed());
            } else {
                force = springDamper.updateForceUser(mass.position, mouseControl.pressed);
            }
            springDamper.updatePoints();

            // Repaint
            throwField.repaint();
        }
    };

    private class ThrowField extends JPanel{
        private static final long serialVersionUID = 1L;
        Color sky_down = new Color(0.4f, 0.6f, 1f);
        Color ground = new Color(0.4f, 0.8f, 0);
        GradientPaint gp = new GradientPaint(0, 0, sky_down, 0, GROUND_HEIGHT_DRAW, Color.WHITE);
        int[] decameters = makeDecameterIndicators();
        int[] meters = makeMeterIndicators();
        int[] half_meters = makeHalfMeterIndicators();
        int mouseOffsetY = (int) mouseOffset.getY();
        BasicStroke basicStroke1 = new BasicStroke(1);
        BasicStroke basicStroke3 = new BasicStroke(3);

        private int [] makeDecameterIndicators(){
            int [] decameters = new int[4];
            for (int i = 0; i < 4; i++) {
                decameters[i] = 10 * i * DISTANCE_SCALE;
            }
            return decameters;
        }

        private int [] makeMeterIndicators() {
            int [] meters = new int[41];
            for (int i = 0; i < 41; i++) {
                meters[i] = i * DISTANCE_SCALE;
            }
            return meters;
        }

        private int [] makeHalfMeterIndicators() {
            int[] halfmeters = new int[41];
            for (int i = 0; i < 41; i++) {
                halfmeters[i] = (int) ((i + 0.5) * DISTANCE_SCALE);
            }
            return halfmeters;
        }

        public void drawArrow(Graphics2D g2d, int x){
            g2d.setStroke(basicStroke3);
            g2d.drawLine(x, GROUND_HEIGHT_DRAW, x + 5, GROUND_HEIGHT_DRAW + 10);
            g2d.drawLine(x, GROUND_HEIGHT_DRAW, x - 5, GROUND_HEIGHT_DRAW + 10);
            g2d.drawLine(x, GROUND_HEIGHT_DRAW, x, GROUND_HEIGHT_DRAW + 30);
            g2d.setStroke(basicStroke1);
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            super.paintComponent(graphics);
            Graphics2D g2d = (Graphics2D) graphics;

            // Draw sky and earth
            g2d.setPaint(gp);
            g2d.fillRect(0, 0, xBound, GROUND_HEIGHT_DRAW);

            g2d.setColor(ground);
            g2d.fillRect(0, GROUND_HEIGHT_DRAW, xBound, GROUND_HEIGHT_DRAW);
            g2d.setColor(Color.black);

            // Draw throw-zone
            if ((ai.playing && ai.pressed) || mouseControl.pressed || !timer.isRunning()){
                g2d.drawRect(Simulation.SPACING_LEFT, THROW_ZONE_HEIGHT - mouseOffsetY, throwZoneWidth * DISTANCE_SCALE, GROUND_HEIGHT_DRAW + mouseOffsetY - THROW_ZONE_HEIGHT);
            }
            // Draw ground
            g2d.drawLine(0, GROUND_HEIGHT_DRAW, xBound, GROUND_HEIGHT_DRAW);

            // Draw the last distance thrown
            drawArrow(g2d, mass.lastDistance);

            // Draw the spring
            if (((ai.playing && ai.pressed) || mouseControl.pressed) && mass.airBorne) {
                springDamper.draw(g2d);
            }

            // Draw the mass
            mass.draw(g2d);

            // Draw distance indicators
            for (int i : decameters){
                g2d.drawLine(i, GROUND_HEIGHT_DRAW, i,GROUND_HEIGHT_DRAW + 30);
            }

            for (int i : meters){
                g2d.drawLine(i, GROUND_HEIGHT_DRAW, i,GROUND_HEIGHT_DRAW + 15);
            }

            for (int i : half_meters){
                g2d.drawLine(i, GROUND_HEIGHT_DRAW, i,GROUND_HEIGHT_DRAW + 8);
            }

            // Draw record
            g2d.setColor(Color.red);
            drawArrow(g2d, mass.recordInt);
        }
    }

}
