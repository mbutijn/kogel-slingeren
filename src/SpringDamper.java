import java.awt.*;

class SpringDamper {
    private final double equilibriumLength, stiffness, damping; // these properties do not change throughout simulation
    private double currentLength, oldLength; // these properties change throughout simulation
    private final Vector attachmentMass, initialAttachment, vector, force;
    private final double[] widths =            {0, 0,    0.25, -0.25, 0.25, -0.25, 0.25, -0.25, 0.25, 0,    0};
    private final double[] heightPercentages = {0, 0.15, 0.2,   0.3,  0.4,   0.5,  0.6,   0.7,  0.8,  0.85, 1.0};
    private final int numberOfElements;
    private final Vector[] points, rotatedPoints;

    SpringDamper(double equilibriumLength, double stiffness, double damping) {
        this.equilibriumLength = equilibriumLength;
        this.stiffness = stiffness;
        this.damping = damping;
        this.numberOfElements = heightPercentages.length;
        this.points = new Vector[numberOfElements];
        this.rotatedPoints = new Vector[numberOfElements];
        this.initialAttachment = new Vector(1, 5);
        this.attachmentMass = new Vector(1, 1);
        force = new Vector(0,0);
        vector = new Vector(0,0);
        initialize();
        updatePoints();
    }

    void initialize() {
        currentLength = equilibriumLength;
        oldLength = equilibriumLength;

        vector.setX(initialAttachment.getX() - attachmentMass.getX());
        vector.setY(initialAttachment.getY() - attachmentMass.getY());
        vector.setDirection();
        vector.setAbs();

        for (int i = 0; i < numberOfElements; i++){
            this.points[i] = new Vector(attachmentMass.getX() + widths[i], attachmentMass.getY() + heightPercentages[i] * equilibriumLength);
            this.rotatedPoints[i] = new Vector(heightPercentages[i] * equilibriumLength, widths[i]);
            this.rotatedPoints[i].setDirection();
            this.rotatedPoints[i].setAbs();

            points[i].setX(points[0].getX() + Math.cos(rotatedPoints[i].getDirection() + vector.getDirection()) * rotatedPoints[i].getAbs());
            points[i].setY(points[0].getY() + Math.sin(rotatedPoints[i].getDirection() + vector.getDirection()) * rotatedPoints[i].getAbs());
        }
    }

    Vector updateForceUser(Vector mass, boolean pressed){
        // user input
        points[0].setX(Simulation.mouseControl.getMouseX());
        points[0].setY(Simulation.mouseControl.getMouseY());

        return updateForce(mass, pressed);
    }

    Vector updateForceAI(Vector attachmentUp, boolean pressed){
        points[0].setX(Simulation.ai.getX());
        points[0].setY(Simulation.ai.getY());

        return updateForce(attachmentUp, pressed);
    }

    Vector updateForce(Vector massPosition, boolean pressed) {
        if (pressed && Simulation.mass.airBorne) {
            // the point where spring is connected to the mass
            points[numberOfElements - 1].setX(massPosition.getX());
            points[numberOfElements - 1].setY(massPosition.getY());

            // calculate the difference vector
            vector.setX(massPosition.getX() - points[0].getX());
            vector.setY(massPosition.getY() - points[0].getY());
            vector.setDirection();
            vector.setAbs();

            oldLength = currentLength;
            currentLength = vector.getAbs();

            // calculate force vector
            force.setDirection(vector.getDirection());
            force.setAbs((currentLength - equilibriumLength) * stiffness + (currentLength - oldLength) * damping / Simulation.samplePeriod);
            force.setXY();

            //System.out.println("["+points[0].getX()+","+points[0].getY()+","+force.getAbs()+"],");

        } else {
            // the spring is not attached to the mass
            force.setX(0);
            force.setY(0);
        }
        return force;
    }

    void updatePoints() {
        for (int i = 1; i < numberOfElements; i++){
            rotatedPoints[i].setX(heightPercentages[i] * currentLength);
            rotatedPoints[i].setDirection();
            rotatedPoints[i].setAbs();

            points[i].setX(points[0].getX() + Math.cos(rotatedPoints[i].getDirection() + vector.getDirection()) * rotatedPoints[i].getAbs());
            points[i].setY(points[0].getY() + Math.sin(rotatedPoints[i].getDirection() + vector.getDirection()) * rotatedPoints[i].getAbs());
        }
    }

    public void draw(Graphics2D g2d) {
        for (int i = 0; i < this.numberOfElements - 1; i++){
            int[] coordinates1 = this.points[i].toPixelIndices(Simulation.yBound);
            int[] coordinates2 = this.points[i+1].toPixelIndices(Simulation.yBound);

            g2d.drawLine(coordinates1[0], coordinates1[1], coordinates2[0], coordinates2[1]);
        }
    }
}
