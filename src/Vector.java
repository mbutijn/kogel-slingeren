class Vector {
    private double x, y, direction, abs;

    Vector(double x, double y){
        this.x = x;
        this.y = y;
    }

    int [] toPixelIndices(int yBound) {
        return new int[]{(int) Math.round(Simulation.DISTANCE_SCALE * x), (int) Math.round(yBound - Simulation.DISTANCE_SCALE * y)};
    }

    void setDirection(double direction){
        this.direction = direction;
    }

    void setDirection(){
        this.direction = Math.atan2(y, x);
    }

    double getDirection(){
        return direction;
    }

    public void setAbs(double abs) {
        this.abs = abs;
    }

    public void setAbs(){
        this.abs = Math.sqrt(x * x + y * y);
    }

    double getAbs(){
        return abs;
    }

    public void setX(double x){
        this.x = x;
    }

    double getX() {
        return x;
    }

    public void setY(double y) {
        this.y = y;
    }

    double getY() {
        return y;
    }

    public void setXY(){
        this.x = abs * Math.cos(direction);
        this.y = abs * Math.sin(direction);
    }
}
