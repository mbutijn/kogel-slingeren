import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RestartButton {
    Simulation simulation;

    public RestartButton(Simulation simulation) {
        this.simulation = simulation;
    }

    public JButton makeButton() {
        JButton resetButton = new JButton("Restart");
        resetButton.setFont(Simulation.font);
        resetButton.addActionListener(new Reset());
        return resetButton;
    }

    private class Reset implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            Simulation.mass.record = 0;
            Simulation.mass.recordInt = 0;
            Simulation.mass.lastDistance = 0;
            Simulation.scoreField.setText("0");
            Simulation.recordField.setText("0");
            Simulation.sizeField.setText("5.0");
            RetryButton.resetMass(simulation);
            Simulation.ai.playing = false;
        }
    }
}
