import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class RetryButton {
    private final Simulation simulation;
    static boolean restarted = false;

    RetryButton(Simulation simulation) {
        this.simulation = simulation;
    }

    JButton makeButton() {
        JButton restartButton = new JButton("Retry");
        restartButton.setFont(Simulation.font);
        restartButton.addActionListener(new Restart());
        return restartButton;
    }

    private class Restart implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            resetMass(simulation);
        }
    }

    public static void resetMass(Simulation simulation){
        String size = Simulation.sizeField.getText();
        double newMass;
        try {
            newMass = Double.parseDouble(size);
        } catch(Exception ex){
            Simulation.sizeField.setText("5.0");
            newMass = 5.0;
            System.out.println("invalid input, mass was set to 5.0");
        }
        newMass = Math.min(20.0, Math.max(1.0, newMass));
        Simulation.sizeField.setText("" + newMass);

        Simulation.mass.setMass(newMass);
        Simulation.mass.setDiameter(Math.sqrt(0.05 * newMass));

        Simulation.mass.initializeVectors();
        simulation.springDamper.initialize();
        Simulation.mouseControl.running = false;
        Simulation.mouseControl.released = false;
        Simulation.timer.start();
        Simulation.sizeField.setEditable(true);
        Simulation.up.setEnabled(true);
        Simulation.down.setEnabled(true);
        restarted = true;
    }
}
