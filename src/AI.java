public class AI {
    double x, y;
    final double X_LIMIT, Y_LIMIT;
    boolean playing;
    boolean pressed;
    Mass mass;

    AI(boolean playing, Mass mass) {
        this.playing = playing;
        this.pressed = playing;
        this.X_LIMIT = Simulation.throwZoneWidth + Simulation.space_left; // = 3
        this.x = X_LIMIT;
        this.Y_LIMIT = Simulation.throwZoneHeight + 28.0 / (double) (Simulation.DISTANCE_SCALE); // = 8.56
        this.y = Y_LIMIT;
        this.mass = mass;
    }

    public void updateAction() {
        if (mass.airBorne) {
            if (pressed) {
                Vector vel = mass.getVelocity();
                vel.setDirection();
                vel.setAbs();
                int startThreshold = 3;
                int endThreshold = 5;
                double accDir = vel.getAbs() < startThreshold ? vel.getDirection()
                        : vel.getAbs() < endThreshold ? vel.getDirection() + 0.5 * Math.PI * (vel.getAbs() - startThreshold)/(endThreshold-startThreshold)
                        : vel.getDirection() + 0.5 * Math.PI;
                double length = 4;

                x = Math.min(X_LIMIT, Math.max(Simulation.space_left, mass.position.getX() + length * Math.cos(accDir)));
                y = Math.min(Y_LIMIT, Math.max(Simulation.groundHeight, mass.position.getY() + length * Math.sin(accDir)));
                if (mass.position.getY() < 3.5 && mass.getVelocity().getY() < 0){
                    y = Y_LIMIT;
                }

                if (vel.getX() > 4 && vel.getY() > 2) {
                    x = X_LIMIT;
                    y = Y_LIMIT;
                }

                if (vel.getX() > 5 && vel.getY() > 3) {
                    setPressed(false);
                }
            }
        } else {
            Simulation.mass.initializeVectors();
            pressed = true;
        }
    }

    public double getX(){
        return x;
    }

    public double getY(){
        return y;
    }

    public void setPressed(boolean pressed){
        this.pressed = pressed;
    }

    public boolean getPressed() {
        return pressed;
    }

}
