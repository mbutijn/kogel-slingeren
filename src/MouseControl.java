import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class MouseControl {
    private final JFrame frame;
    boolean pressed = false;
    boolean running = false;
    boolean released = false;
    private final double ground;
    private final Vector limit = new Vector(Simulation.SPACING_LEFT + Simulation.throwZoneWidth * Simulation.DISTANCE_SCALE, Simulation.THROW_ZONE_HEIGHT);
    private final Vector offset;

    MouseControl(JFrame frame, Vector offset) {
        this.frame = frame;
        this.offset = offset;
        this.ground = Simulation.yBound - Simulation.groundHeight * Simulation.DISTANCE_SCALE + offset.getY();
        frame.addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent event) {
                if (!released) {
                    if (!running) {
                        Simulation.timer.start();
                        Simulation.northPanel.setVisible(false);
                        running = true;
                        Simulation.sizeField.setEditable(false);
                        Simulation.up.setEnabled(false);
                        Simulation.down.setEnabled(false);
                    }
                    pressed = true;
                }
            }

            @Override
            public void mouseReleased(MouseEvent event) {
                pressed = false;
                released = true;
            }

            @Override
            public void mouseClicked(MouseEvent e){
                if (e.getClickCount() == 2){ // Double click
                    Simulation.ai.playing = !Simulation.ai.playing;
                    Simulation.northPanel.setVisible(true);
                    Simulation.retryButton.setEnabled(!Simulation.ai.playing);
                    Simulation.restartButton.setEnabled(!Simulation.ai.playing);
                }
            }
        });

        frame.addMouseWheelListener(e -> Simulation.mass.changeMass(-1*e.getWheelRotation()));
    }

    double getMouseX(){
        double xPosition = getPoint().getX() - frame.getX();
        xPosition = Math.min(limit.getX() + offset.getX(), Math.max(Simulation.SPACING_LEFT+offset.getX(), xPosition));
        return (xPosition - offset.getX()) / Simulation.DISTANCE_SCALE;
    }

    double getMouseY(){
        double yPosition = getPoint().getY() - frame.getY();
        yPosition = Math.min(ground, Math.max(yPosition, limit.getY()));
        return (Simulation.yBound + offset.getY() - yPosition) / Simulation.DISTANCE_SCALE;
    }

    private Point getPoint(){
        return MouseInfo.getPointerInfo().getLocation();
    }
}
