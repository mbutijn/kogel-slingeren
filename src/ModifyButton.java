import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ModifyButton {
    private final int value;
    JButton button;

    ModifyButton(String sign, int value) {
        this.value = value;
        button = new JButton(sign);
        button.setFont(Simulation.font);
        button.setPreferredSize(new Dimension(40, 13));
        button.setMargin(new Insets(5, 5, 5, 5) );
    }

    JButton makeButton() {
        button.addActionListener(new Modify());
        return button;
    }

    private class Modify implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Simulation.mass.changeMass(value);
        }
    }
}
